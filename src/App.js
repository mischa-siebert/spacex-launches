import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import TabbedTable from './components/launches/TabbedTable/TabbedTable.jsx';
import MissionDetails from './components/details/MissionDetails.jsx';

function App() {
  return (
   <Router>
     <Route exact path="/launch/:id" component={MissionDetails} />
     <Route exact path="/" component={TabbedTable} />
   </Router>
  );
}

export default App;
