import { render, screen } from '@testing-library/react';
import TabbedTable from './TabbedTable.jsx';

test('renders Unmanned missions tab', () => {
  const r = render(<TabbedTable />);
  const text = r.getByText(/Unmanned missions/i);
  expect(text).toBeInTheDocument();
});

test('renders Manned missions tab', () => {
  const r = render(<TabbedTable />);
  const text = r.getByText(/Manned missions/);
  expect(text).toBeInTheDocument();
});
