# spacex-launches

spacex-launches is a react.js client for the public `https://api.spacexdata.com/v4/` SpaceX REST API.

It shows all launches of spaceX in a tabular form, separating manned from unmanned missions.
For each table row there is a details view giving information about the flight.


## Docker build & run

1. RUN `docker build . -t sieb:app` from the project directory.

2. RUN `./Run` OR `RUN.bat` depending on your operating system.
    - The Run script simply executes the following command line:
        `docker run -p 3001:3000 sieb:app` 

        Adjust if necessary.

3. The app should be available on `localhost:3001`.

## Run locally

1. RUN `npm install`.

2. RUN `npm start`.

3. The app should be available on `localhost:3000`.

## E2e tests

E2e tests can be found [in this bitbucket repo](https://bitbucket.org/mischa-siebert/spacex-launches-e2e/src/master/).

For setup please refer to the README of the e2e tests repository.


### NOTE:

E2e tests are currently configured to run against the docker container.
Please refer to the README of the e2e tests repository for configuration details.


