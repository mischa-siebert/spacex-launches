import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import { baseUrl } from "../../config/config.js";
import styles from "./MissionDetails.module.css";

export default function MissionDetails(props) {
  const params = useParams();
  const [mission, setMission] = useState({});
  const [crew, setCrew] = useState([]);
  const [capsule, setCapsule] = useState([]);
  const [rocket, setRocket] = useState({});
  const [payloads, setPayloads] = useState([]);

  useEffect(() => {
    fetch(`${baseUrl}launches/${encodeURIComponent(params.id)}`)
      .then((response) => response.json())
      .then((body) => {
        setMission(body);

        if (body.crew.length > 0) {
          body.crew.forEach((member) =>
            fetch(`${baseUrl}crew/${encodeURIComponent(member)}`)
              .then((response) => response.json())
              .then((body) => {
                setCrew((previousCrew) => [...previousCrew, body]);
              })
          );
        }
        if (body.capsules.length > 0) {
          body.capsules.forEach((capsule) =>
            fetch(`${baseUrl}capsules/${encodeURIComponent(capsule)}`)
              .then((response) => response.json())
              .then((body) => {
                setCapsule((previousCapsules) => [...previousCapsules, body]);
              })
          );
        }
        if (body.payloads.length > 0) {
          body.payloads.forEach((payload) =>
            fetch(`${baseUrl}payloads/${encodeURIComponent(payload)}`)
              .then((response) => response.json())
              .then((body) => {
                setPayloads((previousPayloads) => [...previousPayloads, body]);
              })
          );
        }

        fetch(`${baseUrl}rockets/${encodeURIComponent(body.rocket)}`)
          .then((response) => response.json())
          .then((body) => setRocket(body));
      });
  }, [params.id]);

  const hasFlickrImages = (path) => {
    return path ? true : false;
  };

  const getMissionStatus = () => {
    return mission.upcoming
      ? "Upcoming Mission"
      : mission.success
      ? "Successful Mission"
      : "Mission failed";
  };

  return (
    <>
      <ArrowBackIcon
        style={{ fontSize: 45, cursor: "pointer" }}
        onClick={() => props.history.goBack()}
      />
      <h2 style={{ fontFamily: "Impact, sans-serif", paddingLeft: 46  }}>{mission.name}</h2>

      {hasFlickrImages(mission?.links?.flickr?.original)
        ? mission.links.flickr.original
            .slice(0, 3)
            .map((link) => (
              <img
                alt="mission"
                key={link}
                className={styles.missionImg}
                style={{ paddingLeft: 50 }}
                src={link}
              />
            ))
        : null}

      <ul className={styles.info} style={{ width: 500 }}>
        <li>
          <h4 style={{fontFamily: "Impact, sans-serif"}}>
          {getMissionStatus()}

          </h4>
        </li>
        <li>{mission.details}</li>
        <li style={{ display: "flex" }}>
          {rocket?.flickr_images?.length > 0 ? (
            <img
              alt="rocket"
              className={styles.rocketImg}
              src={rocket.flickr_images[0]}
            />
          ) : null}
          <ul>
            <li>{`${rocket.name} ${rocket.type}`}</li>
            <li>
              <a href={rocket.wikipedia} target="_blank" rel="noreferrer">
                wikipedia
              </a>
            </li>
          </ul>
        </li>
        <li>
          {capsule?.length > 0 ? `${capsule[0].type} capsule` : "No capsule"}
        </li>
        <li>
          {payloads.length > 0 ? (
            <>
              <b>Payloads: </b>
              <br />
              {payloads.map((payload) => (
                <div key={payload.id}>
                  <span>{`Transport ${payload.name} (${payload.type}) to ${payload.orbit} orbit`}</span>{" "}
                  <br />
                </div>
              ))}
            </>
          ) : null}
        </li>
      </ul>

      <ul>
        {crew.map((member) => (
          <div key={member.id} style={{ display: "flex" }}>
            <li style={{ paddingTop: 25 }}>
              <img
                alt="crew member"
                className={styles.crewImg}
                src={member.image}
              />
            </li>
            <ul style={{ alignSelf: "center" }}>
              <li>{member.name}</li>
              <li>
                <a href={member.wikipedia} target="_blank" rel="noreferrer">
                  wikipedia
                </a>
              </li>
            </ul>
          </div>
        ))}
      </ul>
    </>
  );
}
