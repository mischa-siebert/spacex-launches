import React from "react";
import { useState } from "react";

import MannedMissionList from "../MissionTable/MannedMissionTable.jsx";
import UnmannedMissionList from "../MissionTable/UnmannedMissionTable.jsx";
import styles from "./TabbedTable.module.css";

export default function TabbedTable(props) {
  const [manned, setManned] = useState(true);

  return (
    <>
      <ul style={{ listStyleType: "none" }}>
        <li
        className={styles.headerTab}
          style={{ display: "inline", cursor: "pointer", marginRight: 150, width: 100}}
          onClick={() => setManned(true)}
        >
        Manned missions
        </li>
        <li
          className={styles.headerTab}
          id="unmanned"
          style={{
            display: "inline",
            cursor: "pointer",
          }}
          onClick={() => setManned(false)}
        >
          Unmanned missions
        </li>
      </ul>
      {manned ? (
        <MannedMissionList navigation={props.history} />
      ) : (
        <UnmannedMissionList navigation={props.history} />
      )}
    </>
  );
}
